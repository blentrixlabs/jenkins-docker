FROM jenkins
USER 0
RUN apt-get update -y && \
    apt-get install ansible rsync -y
RUN wget http://updates.jenkins-ci.org/download/war/2.164.2/jenkins.war && \
    mv ./jenkins.war /usr/share/jenkins && \
    chown jenkins:jenkins /usr/share/jenkins/jenkins.war
